/*
 Copyright © 2022 XRPro, LLC. All rights reserved.
 http://structure.io
 */

import GLKit
import ImageIO
import MessageUI
import MetalKit
import Structure
import StructureKit
import UIKit
import ZipArchive

@objc protocol MeshViewDelegate: NSObjectProtocol {
  func meshViewWillDismiss()
  func meshViewDidDismiss()
  func meshViewDidDismissToQR()
  @discardableResult func meshViewDidRequestColorizing(_ mesh: STMesh, previewCompletionHandler: @escaping () -> Void, enhancedCompletionHandler: @escaping () -> Void) -> Bool
}

@objcMembers
public class MeshViewController: UIViewController, UIGestureRecognizerDelegate {
  @IBOutlet var meshViewerMessageLabel: UILabel!
  @IBOutlet var mtkView: MTKView!
  @IBOutlet var sendButton: UIButton!
  @IBOutlet var sendingActivityIndicator: UIButton!
  @IBOutlet weak var trySaveButton: UIButton!
  @IBOutlet weak var statusLabel: UILabel!
  @IBOutlet weak var restartButton: UIButton!
  
  private var displayLink: CADisplayLink?
  
  private var mtkRenderer: MetalMeshRenderer!
  var restartViewController: RestartScreenViewController?
  var mainViewController: ViewController?
  
  private var viewpointController: ViewpointController = .init()
  var mailViewController: MFMailComposeViewController?
  private var modelViewMatrixBeforeUserInteractions: float4x4?
  private var projectionMatrixBeforeUserInteractions: float4x4?
  var zipPath: URL = URL(fileURLWithPath: "")
  var scanDir: String?
  var splitSection: Int = 1
  var provisionalPath: String = "";
  var provisionalScanDir: String = "";
  var mainScanDir: String = "scaning"
  
  
  var _mesh: STMesh? // swiftlint:disable:this identifier_name
  var mesh: STMesh! {
    get {
      return _mesh
    }
    set {
      _mesh = newValue
      if let mesh = _mesh {
        mtkRenderer.updateMesh(mesh: mesh)
        loadMesh()
        needsDisplay = true
      }
    }
  }
  
  weak var delegate: MeshViewDelegate?
  
  // force the view to redraw.
  var needsDisplay: Bool = true
  var colorEnabled: Bool = true
  
  override public func viewDidLoad() {
    super.viewDidLoad()
    
    setupMetal()
    
    let font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
    let _: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: font]
    
    viewpointController.setScreenSize(screenSizeX: Float(mtkView.frame.size.width), screenSizeY: Float(mtkView.frame.size.height))
    
    loadMesh()
    needsDisplay = true
    splitSection = mainViewController?.splitSection ?? 1
  }
  
  func setupMetal() {
    let device = MTLCreateSystemDefaultDevice()!
    mtkView.device = device
    
    mtkView.colorPixelFormat = .bgra8Unorm
    mtkView.depthStencilPixelFormat = .depth32Float
    
    mtkRenderer = MetalMeshRenderer(view: mtkView, device: device, mesh: mesh, size: view.bounds.size)
    mtkRenderer.viewpointController = viewpointController
    mtkView.delegate = mtkRenderer
    
    // we will trigger drawing by ourselfes
    mtkView.enableSetNeedsDisplay = false
    mtkView.isPaused = true
    
    // allow access to the bytes to write screenshots
    mtkView.framebufferOnly = false
    
    // correct the projection matrix for our viewport
    let viewportSize = view.bounds.size
    let oldProjection = projectionMatrixBeforeUserInteractions!
    let actualRatio = Float(viewportSize.height / viewportSize.width)
    let oldRatio = oldProjection.columns.0.norm() / oldProjection.columns.1.norm()
    let diff = actualRatio / oldRatio
    let newProjection = float4x4.makeScale(diff, 1.0, 1) * oldProjection
    viewpointController.setCameraProjection(newProjection)
  }
  
  override public func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    displayLink?.invalidate()
    displayLink = nil
  }
  
  override open func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    setupDisplayLynk()
    
  }
  
  override public func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    needsDisplay = true
  }
  
  private func setupDisplayLynk() {
    displayLink?.invalidate()
    displayLink = nil
    
    displayLink = CADisplayLink(target: self, selector: #selector(MeshViewController.draw))
    displayLink!.add(to: RunLoop.main, forMode: RunLoop.Mode.common)
    
    viewpointController.reset()
  }
  
  override public var prefersStatusBarHidden: Bool {
    return true
  }
  
  func setCameraProjectionMatrix(_ projection: float4x4) {
    viewpointController.setCameraProjection(projection)
    projectionMatrixBeforeUserInteractions = projection
  }
  
  func resetMeshCenter(_ center: vector_float3, _ size: vector_float3) {
    viewpointController.reset()
    viewpointController.setMeshCenter(center, size)
    modelViewMatrixBeforeUserInteractions = viewpointController.currentGLModelViewMatrix()
  }
  
  func setLabel(_ label: UILabel, enabled: Bool) {
    let whiteLightAlpha = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.5)
    
    if enabled {
      label.textColor = UIColor.white
    } else {
      label.textColor = whiteLightAlpha
    }
  }
  
  @IBAction func dismissView() {
    if delegate?.responds(to: #selector(ViewController.meshViewWillDismiss)) ?? false {
      delegate?.meshViewWillDismiss()
    }
    
    displayLink?.invalidate()
    displayLink = nil
    
    mesh = nil
    
    dismiss(animated: true) {
      if self.delegate?.responds(to: #selector(ViewController.meshViewDidDismiss)) ?? false {
        self.delegate?.meshViewDidDismiss()
        self.delegate = nil
      }
    }
  }
  
  @IBAction func displayControlChanged(_ sender: Any) {
    let meshIsColorized = mesh!.hasPerVertexColors() || mesh!.hasPerVertexUVTextureCoords()
    
    if !meshIsColorized {
      colorizeMesh()
    }
    
    needsDisplay = true
  }
  
  func loadMesh() {
    if mesh!.hasPerVertexUVTextureCoords() {
      mtkRenderer.mode = .texture
    } else if mesh!.hasPerVertexColors() {
      mtkRenderer.mode = .vertexColor
    } else {
      mtkRenderer.mode = .lightedGrey
    }
    
    let meshIsColorized = mesh!.hasPerVertexColors() || mesh!.hasPerVertexUVTextureCoords()
    
    if !meshIsColorized {
      colorizeMesh()
    }
    
    needsDisplay = true
  }
  // MARK: - Touch & Gesture control
  
  @IBAction func pinchScaleGesture(_ gestureRecognizer: UIPinchGestureRecognizer) {
    // Forward to the ViewpointController.
    if gestureRecognizer.state == .began {
      viewpointController.onPinchGestureBegan(Float(gestureRecognizer.scale))
    } else if gestureRecognizer.state == .changed {
      viewpointController.onPinchGestureChanged(Float(gestureRecognizer.scale))
    }
  }
  
  @IBAction func oneFingerPanGesture(_ gestureRecognizer: UIPanGestureRecognizer) {
    let touchPos = gestureRecognizer.location(in: view)
    let touchVel = gestureRecognizer.velocity(in: view)
    let touchPosVec = vector_float2(Float(touchPos.x), Float(touchPos.y))
    let touchVelVec = vector_float2(Float(touchVel.x), Float(touchVel.y))
    
    if gestureRecognizer.state == .began {
      viewpointController.onOneFingerPanBegan(touchPosVec)
    } else if gestureRecognizer.state == .changed {
      viewpointController.onOneFingerPanChanged(touchPosVec)
    } else if gestureRecognizer.state == .ended {
      viewpointController.onOneFingerPanEnded(touchVelVec)
    }
  }
  
  @IBAction func twoFingersPanGesture(_ gestureRecognizer: UIPanGestureRecognizer) {
    if gestureRecognizer.numberOfTouches != 2 {
      return
    }
    
    let touchPos = gestureRecognizer.location(in: view)
    let touchVel = gestureRecognizer.velocity(in: view)
    let touchPosVec = vector_float2(Float(touchPos.x), Float(touchPos.y))
    let touchVelVec = vector_float2(Float(touchVel.x), Float(touchVel.y))
    
    if gestureRecognizer.state == .began {
      viewpointController.onTwoFingersPanBegan(touchPosVec)
    } else if gestureRecognizer.state == .changed {
      viewpointController.onTwoFingersPanChanged(touchPosVec)
    } else if gestureRecognizer.state == .ended {
      viewpointController.onTwoFingersPanEnded(touchVelVec)
    }
  }
  
  @IBAction func trySaveButtonPressed(_ sender: Any) {
    saveMeshCb()
  }
  
  override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    viewpointController.onTouchBegan()
  }
  
  // MARK: - Rendering
  
  func draw() {
    let viewpointChanged = viewpointController.update()
    
    // If nothing changed, do not waste time and resources rendering.
    if !needsDisplay && !viewpointChanged {
      return
    }
    
    mtkView.draw()
    needsDisplay = false
  }
  
  // MARK: - UI Control
  func showMeshViewerMessage(_ msg: String) {
    meshViewerMessageLabel.text = msg
    
    if meshViewerMessageLabel.isHidden == true {
      meshViewerMessageLabel.isHidden = false
      
      meshViewerMessageLabel.alpha = 0.0
      UIView.animate(withDuration: 0.5, animations: {
        self.meshViewerMessageLabel.alpha = 1.0
      })
    }
  }
  
  func hideMeshViewerMessage() {
    UIView.animate(withDuration: 0.5, animations: {
      self.meshViewerMessageLabel.alpha = 0.0
    }, completion: { _ in
      self.meshViewerMessageLabel.isHidden = true
    })
  }
  
  func colorizeMesh() {
    if let mesh = mesh {
      delegate?.meshViewDidRequestColorizing(mesh, previewCompletionHandler: {}, enhancedCompletionHandler: {
        // Hide progress bar.
        self.hideMeshViewerMessage()
        self.saveMeshCb()
      })
    }
  }
  
  func saveMeshCb() {
    let fileManager = FileManager.default
    guard let cacheDirectory = try? fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) else { return }
    
    let scanPath = URL(string: mainViewController?.scanDir ?? "")
    
    let exportExtensions: [Int: String] = [
      STMeshWriteOptionFileFormat.objFile.rawValue: "obj",
      STMeshWriteOptionFileFormat.objFileZip.rawValue: "zip",
      STMeshWriteOptionFileFormat.plyFile.rawValue: "ply",
      STMeshWriteOptionFileFormat.binarySTLFile.rawValue: "stl"
    ]
    
    guard let meshExportFormat = exportExtensions[getMeshExportFormat()] else { return }
    
    // Setup paths and filenames.
    let filename = String.localizedStringWithFormat("Model.%@", meshExportFormat)
    let filePath = scanPath!.appendingPathComponent("\(filename)")
    
    // Request the file
    let options: [AnyHashable: Any] = [
      kSTMeshWriteOptionFileFormatKey: getMeshExportFormat(),
      kSTMeshWriteOptionUseXRightYUpConventionKey: true
    ]
    
    let meshPath = cacheDirectory.appendingPathComponent("\(mainScanDir)/\(filePath)")
    
    guard let meshToSend = mesh else { 
      showMessageAlert("Caution!", message: "Mesh exporting failed.", doneMsg: "Okay")
      setupStatusFile(isSaved: false)
      return
    }
    
    do {
      try meshToSend.write(toFile: meshPath.path, options: options)
    } catch {
      showMessageAlert("Caution!", message: "Mesh exporting failed: \(error.localizedDescription).", doneMsg: "Okay")
      setupStatusFile(isSaved: false)
      return
    }

    let mainPath = cacheDirectory.appendingPathComponent("\(mainScanDir)/\(mainViewController?.scanDir ?? "")")
    
    do {
      try fileManager.createDirectory(at: mainPath, withIntermediateDirectories: true, attributes: nil)
    } catch {
      showMessageAlert("Caution!", message: "Problem creating directory: \(error)", doneMsg: "Okay")
      setupStatusFile(isSaved: false)
      return
    }
    do {
      let acorscansPath = cacheDirectory.appendingPathComponent("acorscans")
      
      do {
        try fileManager.createDirectory(at: acorscansPath, withIntermediateDirectories: true, attributes: nil)
      } catch {
        print("Error creating directory: \(error)")
        setupStatusFile(isSaved: false)
        return
      }
      
      zipPath = acorscansPath.appendingPathComponent("\(mainViewController?.scanDir ?? "").acorscan")
      
      let success = SSZipArchive.createZipFile(atPath: zipPath.path, withContentsOfDirectory: mainPath.path)
      
      if (success) {
        showMessageAlert("Finished", message: "Session is saved", doneMsg: "Okay")
        sendButton.isEnabled = true
        setupStatusFile(isSaved: true)
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd_HHmmss"
        let provisionalDir = dateFormatter.string(from: date)
        let provisionalScanPath = cacheDirectory.appendingPathComponent("\(mainScanDir)/\(provisionalDir)")
        
        let origenQRPhotoPath = cacheDirectory.appendingPathComponent("\(mainScanDir)/\(mainViewController?.scanDir ?? "")/QRImage.jpg")
        let origenQRCodePath  = cacheDirectory.appendingPathComponent("\(mainScanDir)/\(mainViewController?.scanDir ?? "")/QRCode.txt")
        let QRPhotoPath       = provisionalScanPath.appendingPathComponent("QRImage.jpg")
        let QRCodePath        = provisionalScanPath.appendingPathComponent("QRCode.txt")
        
        provisionalScanDir = provisionalDir

        do { try fileManager.createDirectory(at: provisionalScanPath, withIntermediateDirectories: true, attributes: nil) } catch { return }
        
        if fileManager.fileExists(atPath: origenQRPhotoPath.path) {
          try fileManager.copyItem(at: origenQRPhotoPath, to: QRPhotoPath)
        }

        if fileManager.fileExists(atPath: origenQRCodePath.path) {
          try fileManager.copyItem(at: origenQRCodePath, to: QRCodePath)
        }
        
        mainViewController?.scanDir = provisionalScanDir
         
        let removePath = cacheDirectory.appendingPathComponent(mainScanDir)
        try fileManager.removeItem(atPath: removePath.path)
        
      } else { showMessageAlert("Caution", message: "ACORSCAN not saved", doneMsg: "Okay") }
      
    } catch {
      let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
      let tStats = UnsafeMutablePointer<statfs>.allocate(capacity: 1)
      statfs((paths.last! as NSString).utf8String, tStats)
      let totalSpace = Float(tStats.pointee.f_blocks) * Float(tStats.pointee.f_bsize)
      let freeSpace = Float(tStats.pointee.f_bavail) * Float(tStats.pointee.f_bsize)
      
      let displayFileSize = ByteCountFormatter.string(fromByteCount: Int64(freeSpace), countStyle: .file)
      
      let message = "Session not saved, check that the available space is sufficient: \(displayFileSize)"
      
      showMessageAlert("Caution", message: message, doneMsg: "Okay")
      //setupStatusFile(isSaved: false)
    }
    
    restartButton.isEnabled = true
  }
  
  func setupStatusFile(isSaved: Bool) {
    statusLabel.text       = isSaved ? "Saved" : "Not saved"
    statusLabel.textColor  = isSaved ? .green  : .yellow
    trySaveButton.isHidden = isSaved
  }
  
  @IBAction func emailSupport() {
    let recipientEmail = "support@structure.io"
    let subject = "Contact Us"
    let body = ""
    
    if MFMailComposeViewController.canSendMail() {
      mailViewController = MFMailComposeViewController()
      guard let mailVC = mailViewController else {
        showAlert(title: "ERROR!!!", message: "Failed to create the mail composer.")
        return
      }
      mailVC.mailComposeDelegate = self
      mailVC.setSubject(subject)
      mailVC.setToRecipients([recipientEmail])
      present(mailVC, animated: true)
    } else if let emailURL = createEmailUrl(recipient: recipientEmail, subject: subject, body: body) {
      UIApplication.shared.open(emailURL)
    }
  }
  
  @IBAction func restartButtonPressed(_ sender: Any) {
    performSegue(withIdentifier: "segueToRestart", sender: nil)
  }
  
  @IBAction func transferMeshButtonPressed(_ sender: Any) {
    setupIsSendingUI(isSending: true)
    
    let url = "http://192.168.0.107:8001/upload"
    let request = HttpPostMultipart(ipAddress: url)
    
    request.sendFile(urlString: url, filePath: zipPath) { result in
      switch result {
        case .success(let data):
          print("File uploaded successfully. Response: \(String(data: data, encoding: .utf8) ?? "No response")")
          DispatchQueue.main.async {
            self.showMessageAlert("File uploaded successfully", message: "Response: \(String(data: data, encoding: .utf8) ?? "No response")", doneMsg: "Okay")
            self.setupIsSendingUI(isSending: false)
          }
          
        case .failure(let error):
          DispatchQueue.main.async {
            self.showMessageAlert("Caution", message: "Not send: \(error.localizedDescription)", doneMsg: "Okay")
            self.setupIsSendingUI(isSending: false)
          }
          print("File upload failed with error: \(error.localizedDescription)")
      }
    }
  }
  
  func setupIsSendingUI(isSending isSEnding: Bool) {
    self.sendButton.isHidden = isSEnding
    self.sendingActivityIndicator.isHidden = !isSEnding
    
    if isSEnding { showMeshViewerMessage("Sending") }
    else { hideMeshViewerMessage() }
  }
  
  @IBAction func shareMesh(sender: UIBarButtonItem) {
    guard let cacheDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
    
    let exportExtensions: [Int: String] = [
      STMeshWriteOptionFileFormat.objFile.rawValue: "obj",
      STMeshWriteOptionFileFormat.objFileZip.rawValue: "zip",
      STMeshWriteOptionFileFormat.plyFile.rawValue: "ply",
      STMeshWriteOptionFileFormat.binarySTLFile.rawValue: "stl"
    ]
    
    guard let meshExportFormat = exportExtensions[getMeshExportFormat()] else { return }
    
    // Setup paths and filenames.
    let filename = String.localizedStringWithFormat("Model.%@", meshExportFormat)
    let filePath = cacheDirectory.appendingPathComponent("\(filename)")
    
    let screenshotFilename = "Preview.jpg"
    let screenshotPath = cacheDirectory.appendingPathComponent("\(screenshotFilename)")
    
    // Request the file
    let options: [AnyHashable: Any] = [
      kSTMeshWriteOptionFileFormatKey: getMeshExportFormat(),
      kSTMeshWriteOptionUseXRightYUpConventionKey: true
    ]
    
    guard let meshToSend = mesh else { return }
    
    do {
      try meshToSend.write(toFile: filePath.path, options: options)
    } catch {
      showAlert(title: "ERROR!!!", message: "Mesh exporting failed: \(error.localizedDescription).")
      return
    }
    
    // Take a screenshot and save it to disk.
    prepareScreenShot(screenshotPath)
    
    guard let image = UIImage(contentsOfFile: screenshotPath.path) else {
      showAlert(title: "ERROR!!!", message: "Failed to create screenshot of mesh.")
      return
    }
    let meshFile = NSURL.fileURL(withPath: filePath.path)
    let activityItems: [STKMixedActivityItemSource] = [.init(item: .image(image: image)),
                                                       .init(item: .archieve(file: meshFile))]
    let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
    if UIDevice.current.userInterfaceIdiom == .pad {
      activityViewController.popoverPresentationController?.barButtonItem = sender
    }
    
    present(activityViewController, animated: true)
  }
  
  @IBAction func openDeveloperPortal(_ sender: Any) {
    UIApplication.shared.open(URL(string: "https://structure.io/developers")!)
  }
  
  // MARK: - Helper Functions
  
  func prepareScreenShot(_ screenshotPath: URL) {
    let lastDrawableDisplayed = mtkView.currentDrawable?.texture
    if let imageRef = lastDrawableDisplayed?.toImage() {
      let uiImage = UIImage(cgImage: imageRef)
      if let data = uiImage.jpegData(compressionQuality: 0.8) {
        try? data.write(to: screenshotPath)
      }
    }
  }
  
  func getMeshExportFormat() -> Int {
    // Get the format from settings.bundle
    let val = UserDefaults.standard.integer(forKey: "meshExportFormat")
    return val
  }
  
  public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "segueToRestart" {
      restartViewController = segue.destination as? RestartScreenViewController
      restartViewController?.delegate = self
      restartViewController?.modalPresentationStyle = .formSheet
    }
  }
  
  func showMessageAlert(_ title: String, message: String, doneMsg: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let okButton = UIAlertAction(title: doneMsg, style: .default) { _ in
      // Handle action
    }
    
    alert.addAction(okButton)
    present(alert, animated: true, completion: nil)
  }
}
  
extension MeshViewController: RestartScreenDelegate{
  //TODO: navigate to QR view
  func QRViewNavigate() {
    if delegate?.responds(to: #selector(ViewController.meshViewWillDismiss)) ?? false {
      delegate?.meshViewWillDismiss()
    }
    
    displayLink?.invalidate()
    displayLink = nil
    
    mesh = nil
    
    mainViewController?.showAppStatusMessage("Loading...")
    
    do { try FileManager.default.removeItem(atPath: self.provisionalScanDir) } catch {}
    
    dismiss(animated: true) {
      if self.delegate?.responds(to: #selector(MeshViewDelegate.meshViewDidDismissToQR)) ?? false {
        self.delegate?.meshViewDidDismissToQR()
      }
    }
  }
  
  func viewControllerNavigate() {
    //qrViewController.scanDir = provisionalScanDir
    
    if delegate?.responds(to: #selector(ViewController.meshViewWillDismiss)) ?? false {
      delegate?.meshViewWillDismiss()
    }
    
    displayLink?.invalidate()
    displayLink = nil
    
    mesh = nil
    
    dismiss(animated: true) {
      if self.delegate?.responds(to: #selector(ViewController.meshViewDidDismiss)) ?? false {
        self.delegate?.meshViewDidDismiss()
        self.delegate = nil
      }
    }
  }
}
