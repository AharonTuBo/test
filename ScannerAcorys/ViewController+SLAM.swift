/*
 Copyright © 2022 XRPro, LLC. All rights reserved.
 http://structure.io
 */

import AVFoundation
import Structure
import UIKit


extension ViewController {
  func imageFromSampleBuffer(_ sampleBuffer: CMSampleBuffer?) -> UIImage? {
    guard let sampleBuffer = sampleBuffer, let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
      return nil
    }
    
    let ciImage = CIImage(cvPixelBuffer: imageBuffer)
    let temporaryContext = CIContext(options: nil)
    guard let videoImage = temporaryContext.createCGImage(ciImage, from: CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(imageBuffer), height: CVPixelBufferGetHeight(imageBuffer))) else {
      return nil
    }
    
    let image = UIImage(cgImage: videoImage)
    return image
  }
  
  
  func saveSnapshotsYaml() {
    snapshotsYaml.append("%YAML:1.0\n---\n")
    snapshotsYaml.append("images:\n")
    snapshotsYaml.append(listImages)
    snapshotsYaml.append("camera_poses:\n")
    snapshotsYaml.append(listCameraPoses)
    snapshotsYaml.append("camera_intrinsics: intrinsics.yaml\n")
    
    // Crear la ruta del sistema de archivos.
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    
    // Guardar las cámaras de instantáneas en un archivo
    if let snapshotsDir = paths.first {
      let intrinsicsPath = (snapshotsDir as NSString).appendingPathComponent("\(snapshotsDir)/snapshot.yaml")
      do {
        try snapshotsYaml.write(toFile: intrinsicsPath, atomically: true, encoding: .utf8)
      } catch {
        print("Error al escribir en el archivo: \(error)")
      }
    }
  }
  
  func yamlIntrinsicsFromColorFrame(_ colorFrame: STColorFrame) -> String? {
    // OpenCV documentation about camera calibration parameters
    // https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html
    let intrinsics = colorFrame.intrinsics
    //print("Intrinsics: cx: \(intrinsics.cx), cy: \(intrinsics.cy), fx: \(intrinsics.fx), fy: \(intrinsics.fy), height: \(intrinsics.height), k1: \(intrinsics.k1), k2: \(intrinsics.k2), width: \(intrinsics.width)")
    
    var intrinsicsYaml = ""
    intrinsicsYaml += "%YAML:1.0\n---\n"
    intrinsicsYaml += "image_width: \(colorFrame.width)\n"
    intrinsicsYaml += "image_height: \(colorFrame.height)\n"
    intrinsicsYaml += "camera_matrix: !!opencv-matrix\n"
    intrinsicsYaml += "  rows: 3\n"
    intrinsicsYaml += "  cols: 3\n"
    intrinsicsYaml += "  dt: d\n"
    intrinsicsYaml += "  data: [\(intrinsics().fx), 0.0, \(intrinsics().cx), 0.0, \(intrinsics().fy), \(intrinsics().cy), 0.0, 0.0, 1.0]\n"
    intrinsicsYaml += "distortion_model: plumb_bob\n"
    intrinsicsYaml += "distortion_coefficients: !!opencv-matrix\n"
    intrinsicsYaml += "  rows: 5\n"
    intrinsicsYaml += "  cols: 1\n"
    intrinsicsYaml += "  dt: d\n"
    intrinsicsYaml += "  data: [\(intrinsics().k1), \(intrinsics().k2), 0.0, 0.0, 0.0]\n"
    intrinsicsYaml += "rectification_matrix: !!opencv-matrix\n"
    intrinsicsYaml += "  rows: 3\n"
    intrinsicsYaml += "  cols: 3\n"
    intrinsicsYaml += "  dt: d\n"
    intrinsicsYaml += "  data: [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]\n"
    intrinsicsYaml += "projection_matrix: !!opencv-matrix\n"
    intrinsicsYaml += "  rows: 3\n"
    intrinsicsYaml += "  cols: 4\n"
    intrinsicsYaml += "  dt: d\n"
    intrinsicsYaml += "  data: [\(intrinsics().fx), 0.0, \(intrinsics().cx), 0.0, 0.0, \(intrinsics().fy), \(intrinsics().cy), 0.0, 0.0, 0.0, 1.0, 0.0]\n"
    
    return intrinsicsYaml
  }
  
  
  // MARK: - Utilities
  
  func deltaRotationAngleBetweenPoses(inDegrees previousPose: GLKMatrix4, newPose: GLKMatrix4) -> Float {
    let deltaPose = GLKMatrix4Multiply(newPose, // Transpose is equivalent to inverse since we will only use the rotation part.
                                       GLKMatrix4Transpose(previousPose))
    
    // Get the rotation component of the delta pose
    let deltaRotationAsQuaternion = GLKQuaternionMakeWithMatrix4(deltaPose)
    
    // Get the angle of the rotation
    let angleInDegree: Float = GLKQuaternionAngle(deltaRotationAsQuaternion) / .pi * 180
    
    return angleInDegree
  }
  
  func computeTrackerMessage(_ hints: STTrackerHints) -> String? {
    if hints.trackerIsLost {
      return "Tracking Lost! Please Realign or Press Reset."
    }
    
    if hints.modelOutOfView {
      return "Please put the model back in view."
    }
    
    if hints.sceneIsTooClose {
      return "Too close to the scene! Please step back."
    }
    
    return nil
  }
  
  // MARK: - SLAM
  
  func setupSLAM() {
    if slamState.initialized {
      return
    }
    
    // Initialize the scene.
    slamState.scene = STScene()
    
    NSLog("ObjectScanning Tracker is selected")
    
    // Initialize the camera pose tracker.
    let trackerOptions = [
      kSTTrackerTypeKey: dynamicOptions.depthAndColorTrackerIsOn ? NSNumber(value: STTrackerType.depthAndColorBased.rawValue) : NSNumber(value: STTrackerType.depthBased.rawValue),
      kSTTrackerTrackAgainstModelKey: NSNumber(value: true) /* tracking against the model is much better for close range scanning. */,
      kSTTrackerQualityKey: NSNumber(value: STTrackerQuality.accurate.rawValue),
      kSTTrackerBackgroundProcessingEnabledKey: NSNumber(value: true),
      kSTTrackerSceneTypeKey: NSNumber(value: STTrackerSceneType.object.rawValue),
      kSTTrackerLegacyKey: NSNumber(value: true)
    ]
    
    // Initialize the camera pose tracker.
    if let scene = slamState.scene {
      slamState.tracker = STTracker(scene: scene, options: trackerOptions)
    }
    
    // The mapper will be initialized when we start scanning.
    
    // Setup the cube placement initializer.
    if fixedCubePosition {
      slamState.cameraPoseInitializer = STCameraPoseInitializer(volumeSizeInMeters: options.volumeSizeInMeters.toGLK(), options: [
        kSTCameraPoseInitializerStrategyKey: NSNumber(value: STCameraPoseInitializerStrategy.gravityAlignedAtVolumeCenter.rawValue)
      ])
    } else {
      slamState.cameraPoseInitializer = STCameraPoseInitializer(volumeSizeInMeters: options.volumeSizeInMeters.toGLK(), options: [
        kSTCameraPoseInitializerStrategyKey: NSNumber(value: STCameraPoseInitializerStrategy.tableTopCube.rawValue)
      ])
    }
    
    // Set up the initial volume size.
    adjustVolumeSize(options.volumeSizeInMeters)
    
    // Start with cube placement mode
    enterCubePlacementState()
    
    let keyframeManagerOptions = [
      kSTKeyFrameManagerMaxSizeKey: NSNumber(value: options.maxNumKeyFrames),
      kSTKeyFrameManagerMaxDeltaTranslationKey: NSNumber(value: options.maxKeyFrameTranslation),
      kSTKeyFrameManagerMaxDeltaRotationKey: NSNumber(value: options.maxKeyFrameRotation)
    ]
    
    slamState.keyFrameManager = STKeyFrameManager(options: keyframeManagerOptions)
    
    depthAsRgbaVisualizer = STDepthToRgba(options: [
      kSTDepthToRgbaStrategyKey: NSNumber(value: STDepthToRgbaStrategy.gray.rawValue)
    ])
    
    slamState.initialized = true
  }
  
  func resetSLAM() {
    slamState.prevFrameTimeStamp = -1.0
    slamState.mapper?.reset()
    slamState.tracker?.reset()
    slamState.scene?.clear()
    slamState.keyFrameManager?.clear()
    
    enterCubePlacementState()
  }
  
  func clearSLAM() {
    slamState.initialized = false
    slamState.scene = nil
    slamState.tracker = nil
    slamState.mapper = nil
    slamState.keyFrameManager = nil
  }
  
  func setupMapper() {
    if slamState.mapper != nil {
      slamState.mapper = nil // make sure we first remove a previous mapper.
    }
    
    // Here, we set a larger volume bounds size when mapping in high resolution.
    let lowResolutionVolumeBounds: Float = 125
    let highResolutionVolumeBounds: Float = 200
    
    var voxelSizeInMeters = options.volumeSizeInMeters.x / (dynamicOptions.highResMapping ? highResolutionVolumeBounds : lowResolutionVolumeBounds)
    
    // Avoid voxels that are too small - these become too noisy.
    voxelSizeInMeters = keep(inRange: voxelSizeInMeters, minValue: 0.003, maxValue: 0.2)
    
    // Compute the volume bounds in voxels, as a multiple of the volume resolution.
    var volumeBounds = GLKVector3()
    volumeBounds.x = roundf(options.volumeSizeInMeters.x / voxelSizeInMeters)
    volumeBounds.y = roundf(options.volumeSizeInMeters.y / voxelSizeInMeters)
    volumeBounds.z = roundf(options.volumeSizeInMeters.z / voxelSizeInMeters)
    
    print(String(format: "[Mapper] volumeSize (m): %f %f %f volumeBounds: %.0f %.0f %.0f (resolution=%f m)", options.volumeSizeInMeters.x, options.volumeSizeInMeters.y, options.volumeSizeInMeters.z, volumeBounds.x, volumeBounds.y, volumeBounds.z, voxelSizeInMeters))
    
    let mapperOptions = [
      kSTMapperLegacyKey: NSNumber(value: !dynamicOptions.improvedMapperIsOn),
      kSTMapperVolumeResolutionKey: NSNumber(value: voxelSizeInMeters),
      kSTMapperVolumeBoundsKey: [NSNumber(value: volumeBounds.x), NSNumber(value: volumeBounds.y), NSNumber(value: volumeBounds.z)],
      kSTMapperVolumeHasSupportPlaneKey: NSNumber(value: slamState.cameraPoseInitializer!.lastOutput.hasSupportPlane.boolValue),
      kSTMapperEnableLiveWireFrameKey: NSNumber(value: false)
    ] as [String: Any]
    if let scene = slamState.scene {
      slamState.mapper = STMapper(scene: scene, options: mapperOptions)
    }
  }
  
  func yamlCameraPose(wTcam: GLKMatrix4) -> String? {
    var cameraPoseYaml = "  - !!opencv-matrix\n"
    cameraPoseYaml += "    rows: 4\n"
    cameraPoseYaml += "    cols: 4\n"
    cameraPoseYaml += "    dt: d\n"
    cameraPoseYaml += String(format: "    data: [%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f,  %f, %f, %f, %f]\n",
                             wTcam.m00, wTcam.m01, wTcam.m02, wTcam.m03,
                             wTcam.m10, wTcam.m11, wTcam.m12, wTcam.m13,
                             wTcam.m20, wTcam.m21, wTcam.m22, wTcam.m23,
                             wTcam.m30, wTcam.m31, wTcam.m32, wTcam.m33)
    
    if cameraPoseYaml.isEmpty {
      return nil
    }
    
    return cameraPoseYaml
  }
  
  
  func maybeAddKeyframe(with depthFrame: STDepthFrame, colorFrame: STColorFrame?, depthCameraPoseBeforeTracking: GLKMatrix4) -> String? {
    if colorFrame == nil {
      return nil // nothing to do
    }
    
    var depthCameraPoseAfterTracking = GLKMatrix4Identity
    
    depthCameraPoseAfterTracking = slamState.tracker!.lastFrameCameraPose()
    
    // Make sure the pose is in color camera coordinates in case we are not using registered depth.
    let iOSColorFromDepthExtrinsic = depthFrame.iOSColorFromDepthExtrinsics()
    let colorCameraPoseAfterTracking = GLKMatrix4Multiply(depthCameraPoseAfterTracking, GLKMatrix4Invert(iOSColorFromDepthExtrinsic, nil))
    
    var showHoldDeviceStill = false
    
    // Check if the viewpoint has moved enough to add a new keyframe
    // OR if we don't have a keyframe yet
    if slamState.keyFrameManager!.wouldBeNewKeyframe(withColorCameraPose: colorCameraPoseAfterTracking) {
      let isFirstFrame = slamState.prevFrameTimeStamp < 0.0
      var canAddKeyframe = false
      
      if isFirstFrame {
        canAddKeyframe = true
      } else {
        var deltaAngularSpeedInDegreesPerSecond = Float.greatestFiniteMagnitude
        let deltaSeconds: TimeInterval = depthFrame.timestamp - slamState.prevFrameTimeStamp
        deltaAngularSpeedInDegreesPerSecond = deltaRotationAngleBetweenPoses(inDegrees: depthCameraPoseBeforeTracking, newPose: depthCameraPoseAfterTracking) / Float(deltaSeconds)
        
        // If the camera moved too much since the last frame, we will likely end up
        // with motion blur and rolling shutter, especially in case of rotation. This
        // checks aims at not grabbing keyframes in that case.
        if deltaAngularSpeedInDegreesPerSecond < options.maxKeyframeRotationSpeed {
          canAddKeyframe = true
        }
      }
      
      if canAddKeyframe {
        slamState.keyFrameManager!.processKeyFrameCandidate(withColorCameraPose: colorCameraPoseAfterTracking, colorFrame: colorFrame, depthFrame: nil) // Spare the depth frame memory, since we do not need it in keyframes.
        
        DispatchQueue.main.async {
          // Crear la ruta del sistema de archivos.
          let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
          
          // Obtener pose de la cámara con respecto al marco de referencia de la aplicación.a
          // OpenGL ES supone que las matrices tienen un orden mayor de columna.
          var wTcam = GLKMatrix4Transpose(colorCameraPoseAfterTracking)
          // Se requiere rotación alrededor del eje X para que coincidan los marcos.
          wTcam = GLKMatrix4Multiply(wTcam, GLKMatrix4MakeXRotation(.pi))
          
          let yamlCameraPose = self.yamlCameraPose(wTcam: wTcam)
          
          self.listImages = self.listImages.appending(String(format: "  - \"keyframe_%03d.png\"\n", self.indKeyframes))
          self.listCameraPoses = self.listCameraPoses.appending(yamlCameraPose!)
          
          // Convertir parámetros intrínsecos de la cámara
          let yamlIntrinsics = self.yamlIntrinsicsFromColorFrame(colorFrame!)
          
          // Guardar parámetros intrínsecos en un archivo
          let intrinsicsPath = (paths[0] as NSString).appendingPathComponent("/\(self.snapshotsDir)/intrinsics.yaml")

          try? yamlIntrinsics?.write(toFile: intrinsicsPath, atomically: true, encoding: .utf8)
          
          // Convertir el fotograma de color
          guard let image = self.imageFromSampleBuffer(colorFrame?.sampleBuffer) else { return }
          
          print("Imagen creada \(self.indKeyframes)")
          
          let imagePath = (paths[0] as NSString).appendingPathComponent("/\(self.snapshotsDir)/keyframe_\(String(format: "%03d", self.indKeyframes)).png")

          // Guardar imagen.
          try? image.pngData()?.write(to: URL(fileURLWithPath: imagePath))
          
          self.indKeyframes += 1
          
          print("Arucos \(self.indKeyframes):")
          
          self.registerAruco(image)
        }
      } else {
        // Moving too fast. Hint the user to slow down to capture a keyframe
        // without rolling shutter and motion blur.
        showHoldDeviceStill = true
      }
    }
    
    if showHoldDeviceStill {
      return "Please hold still so we can capture a keyframe..."
    }
    
    return nil
  }

  func updateMeshAlpha(for poseAccuracy: STTrackerPoseAccuracy) {
    switch poseAccuracy {
      case STTrackerPoseAccuracy.high, STTrackerPoseAccuracy.approximate:
        metalData.meshRenderingAlpha = 0.8
      case STTrackerPoseAccuracy.low:
        metalData.meshRenderingAlpha = 0.4
      case STTrackerPoseAccuracy.veryLow, STTrackerPoseAccuracy.notAvailable:
        metalData.meshRenderingAlpha = 0.1
      default:
        print("STTracker unknown pose accuracy.")
    }
  }
  
  func processDepthFrame(_ depthFrame: STDepthFrame, colorFrameOrNil colorFrame: STColorFrame?) {
    if options.applyExpensiveCorrectionToDepth {
      let couldApplyCorrection = depthFrame.applyExpensiveCorrection()
      if !couldApplyCorrection {
        print("Warning: could not improve depth map accuracy, is your firmware too old?")
      }
    }
    
    if runDepthRefinement {
      depthFrame.applyDepthRefinement()
    }
    
    switch slamState.scannerState {
      case ScannerState.cubePlacement:
        var depthFrameForCubeInitialization = depthFrame
        var depthCameraPoseInColorCoordinateFrame = GLKMatrix4Identity
        
        // If we are using color images but not using registered depth, then use a registered
        // version to detect the cube, otherwise the cube won't be centered on the color image,
        // but on the depth image, and thus appear shifted.
        if useColorCamera {
          let iOSColorFromDepthExtrinsic = depthFrame.iOSColorFromDepthExtrinsics()
          depthCameraPoseInColorCoordinateFrame = iOSColorFromDepthExtrinsic
          depthFrameForCubeInitialization = depthFrame.registered(to: colorFrame)
        }
        
        // Estimate the new scanning volume position.
        if GLKVector3Length(lastGravity) > 1e-5 {
          do {
            try slamState.cameraPoseInitializer!.updateCameraPose(withGravity: lastGravity, depthFrame: depthFrameForCubeInitialization)
            // Since we potentially detected the cube in a registered depth frame, also save the pose
            // in the original depth sensor coordinate system since this is what we'll use for SLAM
            // to get the best accuracy.
            slamState.initialDepthCameraPose = GLKMatrix4Multiply(slamState.cameraPoseInitializer!.lastOutput.cameraPose, depthCameraPoseInColorCoordinateFrame)
          } catch {
            NSLog("Camera pose initializer error.")
          }
        }
        
        // Enable the scan button if the pose initializer could estimate a pose.
        scanButton.isEnabled = slamState.cameraPoseInitializer!.lastOutput.hasValidPose.boolValue
        
      case ScannerState.scanning:
        // First try to estimate the 3D pose of the new frame.
        
        var trackingMessage: String?
        var keyframeMessage: String?
        
        //            var depthCameraPoseBeforeTracking = GLKMatrix4Identity
        
        let depthCameraPoseBeforeTracking = slamState.tracker!.lastFrameCameraPose()
        
        // Integrate it into the current mesh estimate if tracking was successful.
        do {
          try slamState.tracker!.updateCameraPose(with: depthFrame, colorFrame: colorFrame)
          
          // Update the tracking message.
          trackingMessage = computeTrackerMessage(slamState.tracker!.trackerHints)
          
          // Set the mesh transparency depending on the current accuracy.
          updateMeshAlpha(for: slamState.tracker!.poseAccuracy)
          
          // If the tracker accuracy is high, use this frame for mapper update and maybe as a keyframe too.
          if slamState.tracker!.poseAccuracy.rawValue >= STTrackerPoseAccuracy.high.rawValue {
            slamState.mapper?.integrateDepthFrame(depthFrame, cameraPose: (slamState.tracker?.lastFrameCameraPose())!)
          }
          keyframeMessage = maybeAddKeyframe(with: depthFrame, colorFrame: colorFrame, depthCameraPoseBeforeTracking: depthCameraPoseBeforeTracking)
          
          // Tracking messages have higher priority.
          if trackingMessage != nil {
            showTrackingMessage(trackingMessage!)
          } else if keyframeMessage != nil {
            showTrackingMessage(keyframeMessage!)
          } else {
            hideTrackingErrorMessage()
          }
        } catch let trackingError as NSError {
          NSLog("[Structure] STTracker Error: %@.", trackingError.localizedDescription)
          
          trackingMessage = trackingError.localizedDescription
        }
        slamState.prevFrameTimeStamp = depthFrame.timestamp
        
      case ScannerState.viewing:
        // Do nothing, the MeshViewController will take care of this.
        break
      default:
        break
    }
  }
}
