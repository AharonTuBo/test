//
//  HttpPostMultipart.swift
//  ScannerAcorys
//
//  Created by Corify Care on 13/5/24.
//

import Foundation
import MobileCoreServices

class HttpPostMultipart {
  var request: URLRequest?
  var urlString: String = ""
  var headers: [String: String] = [:]
  var fields: [String: String] = [:]
  var files: [String: URL] = [:]
  var ipAddress: String
  var timer: Timer?
  var session: URLSession?
  
  func addHeader(key: String, value: String) {
    headers[key] = value
  }
  
  func addFormField(key: String, value: String) {
    fields[key] = value
  }
  
  func addFormFile(key: String, path: URL) {
    files[key] = path
  }
  
  func getMimeType(for path: String) -> String {
      let url = URL(fileURLWithPath: path)
      let pathExtension = url.pathExtension as CFString

      guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension, nil)?.takeRetainedValue(),
            let mimeType = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() else {
          return "application/octet-stream"
      }

      return mimeType as String
  }
  
  func sendFile(urlString: String, filePath: URL, completion: @escaping (Result<Data, Error>) -> Void) {
      guard let url = URL(string: urlString) else {
          print("Invalid URL")
          return
      }

      var request = URLRequest(url: url)
      request.httpMethod = "POST"

      let boundary = "Boundary-\(UUID().uuidString)"
      request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

      let fileName = filePath.lastPathComponent
      let mimeType = filePath
      var body = Data()

      // Add the file data to the body
      body.append("--\(boundary)\r\n".data(using: .utf8)!)
      body.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(fileName)\"\r\n".data(using: .utf8)!)
      body.append("Content-Type: \(mimeType)\r\n\r\n".data(using: .utf8)!)
      if let fileData = try? Data(contentsOf: filePath) {
          body.append(fileData)
      }
      body.append("\r\n".data(using: .utf8)!)

      // Add the closing boundary
      body.append("--\(boundary)--\r\n".data(using: .utf8)!)

      // Set the body to the request
      request.httpBody = body

      let session = URLSession.shared
      let task = session.dataTask(with: request) { data, response, error in
          if let error = error {
              completion(.failure(error))
              return
          }

          guard let data = data else {
              let error = NSError(domain: "NoDataError", code: -1, userInfo: nil)
              completion(.failure(error))
              return
          }

          completion(.success(data))
      }

      task.resume()
  }

  func finish(callback: @escaping (Data?, URLResponse?, Error?) -> Void) {
    guard let url = URL(string: ipAddress) else { return }
    
    request?.url = url
    request?.httpMethod = "POST"
    
    for (key, value) in headers {
      request?.addValue(value, forHTTPHeaderField: key)
    }
    
    let boundary = "unique-consistent-string"
    let contentType = "multipart/form-data; boundary=\(boundary)"
    request?.setValue(contentType, forHTTPHeaderField: "Content-Type")
    
    var body = Data()
    
    for (key, value) in fields {
      if let keyData = "--\(boundary)\r\n".data(using: .utf8),
         let dispositionData = "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8),
         let valueData = "\(value)\r\n".data(using: .utf8) {
        body.append(keyData)
        body.append(dispositionData)
        body.append(valueData)
      }
    }
    
    for (key, path) in files {
      let fileName = (path as URL).lastPathComponent
      let fileExtension = (path as URL).pathExtension
      let contentType = getMimeType(fileExtension: fileExtension)
      
      if let keyData = "--\(boundary)\r\n".data(using: .utf8),
         let dispositionData = "Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(fileName)\"\r\n".data(using: .utf8),
         let contentTypeData = "Content-Type: \(contentType)\r\n\r\n".data(using: .utf8),
         let fileData = try? Data(contentsOf: path),
         let lineBreakData = "\r\n".data(using: .utf8) {
        body.append(keyData)
        body.append(dispositionData)
        body.append(contentTypeData)
        body.append(fileData)
        body.append(lineBreakData)
      }
    }
    
    if !body.isEmpty {
      if let boundaryData = "--\(boundary)--\r\n".data(using: .utf8),
         let _ = String(body.count).data(using: .utf8) {
        body.append(boundaryData)
        request?.httpBody = body
        
        request?.setValue(String(body.count), forHTTPHeaderField: "Content-Length")
      }
    }
    
    let completionHandler: (Data?, URLResponse?, Error?) -> Void = { data, response, error in
      callback(data, response, error)
    }
    
    URLSession.shared.dataTask(with: (request ?? URLRequest(url: URL(fileURLWithPath: ""))) as URLRequest, completionHandler: completionHandler).resume()
  }
  
  private func getMimeType(fileExtension: String) -> String {
    guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension as CFString, nil)?.takeRetainedValue(),
          let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() else {
      return "application/octet-stream"
    }
    return mimetype as String
  }
  
  init(ipAddress: String) {
    self.ipAddress = ipAddress
  }
  
  func startChecking() {
    // Configura el temporizador para que se ejecute cada x segundos (por ejemplo, cada 10 segundos)
    timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(checkIPAddress), userInfo: nil, repeats: true)
    
    // Inicia una sesión de URLSession para realizar la solicitud HTTP
    let config = URLSessionConfiguration.default
    session = URLSession(configuration: config)
  }
  
  @objc func checkIPAddress() {
    let urlString = "http://\(ipAddress)"
    if let url = URL(string: urlString) {
      let dataTask = session?.dataTask(with: url) { data, response, error in
        if let error = error {
          print("La dirección IP \(self.ipAddress) no está disponible. Error: \(error.localizedDescription)")
        } else {
          print("La dirección IP \(self.ipAddress) está disponible.")
        }
      }
      dataTask?.resume()
    }
  }
  
  func stopChecking() {
    timer?.invalidate()
    timer = nil
  }
}
