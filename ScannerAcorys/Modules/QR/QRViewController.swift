import UIKit
import AVFoundation

class QRViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate, AVCapturePhotoCaptureDelegate {
  
  @IBOutlet weak var cameraView: UIImageView!
  @IBOutlet weak var qrTxt: UITextField!
  @IBOutlet weak var qrStartedLabel: UILabel!
  @IBOutlet weak var cameraButton: UIButton!
  @IBOutlet weak var infoButtonLabel: UILabel!
  
  private var qrCode: String = ""
  private var qrCaptureSession: AVCaptureSession!
  private var qrVideoPreviewLayer: AVCaptureVideoPreviewLayer!
  private var qrStillImageOutput: AVCapturePhotoOutput!
  private var backCamera: AVCaptureDevice!
  private var scanButton: UIBarButtonItem!
  public var scanDir: String = ""
  var mainScanDir: String = "scaning"
  
  static func viewController() -> QRViewController {
    let vc = QRViewController(nibName: "QRViewController", bundle: nil)
    return vc
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    qrStartedLabel.isHidden = false
    cameraButton.isHidden = true
    infoButtonLabel.isHidden = true
    qrTxt.isHidden = true
    
    setupUI()
  }
  
  func setupUI() {
    qrTxt.delegate = self
    qrStartedLabel.layer.cornerRadius = 10.0
    qrStartedLabel.layer.masksToBounds = true
    setupQRSession()
    setupCamera()
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    
    // Clear for next iteration
    qrCode = ""
    
    // Stop camera capture session
    qrCaptureSession.stopRunning()
    
    if let backCamera = AVCaptureDevice.default(for: AVMediaType.video) {
      do {
        try backCamera.lockForConfiguration()
        backCamera.exposureMode = .continuousAutoExposure
        backCamera.whiteBalanceMode = .continuousAutoWhiteBalance
        backCamera.focusMode = .continuousAutoFocus
        backCamera.unlockForConfiguration()
      } catch {
        print("Unable to lock device for auto exposure and white balance configuration: \(error.localizedDescription)")
      }
    }
  }
  
  func setupCamera() {
    if let backCamera = AVCaptureDevice.default(for: AVMediaType.video) {
      do {
        try backCamera.lockForConfiguration()
        backCamera.focusMode = .continuousAutoFocus
        backCamera.unlockForConfiguration()
      } catch {
        print("Unable to lock device for exposure and white balance configuration: \(error.localizedDescription)")
      }
    }
  }
  
  // Setup back camera for QR detection
  func setupQRSession() {
    qrCode = ""
    qrTxt.text = ""
    
    scanButton = UIBarButtonItem(title: "Scan", style: .plain, target: self, action: #selector(dismissView))
    navigationItem.rightBarButtonItem = scanButton
    scanButton.isEnabled = false
    
    // Enable visualization
    view.addSubview(cameraView)
    view.addSubview(cameraButton)
    view.addSubview(qrTxt)
    view.addSubview(qrStartedLabel)
    view.addSubview(infoButtonLabel)
    
    qrStartedLabel.isHidden = false
    cameraButton.isHidden = true
    infoButtonLabel.isHidden = true
    qrTxt.isHidden = true
    
    // Setup camera
    qrCaptureSession = AVCaptureSession()
    qrCaptureSession.sessionPreset = AVCaptureSession.Preset.photo
    
    guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video) else {
      print("Unable to access back camera!")
      return
    }
    
    do {
      // Set default camera configuration
      try backCamera.lockForConfiguration()
      //new
      guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
        print("Error: Unable to initialize back camera 1")
        return }
      let videoInput: AVCaptureDeviceInput
      
      do {
        videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
      } catch {
        print("Error: Unable to initialize back camera 2")
        return
      }
      
      if (qrCaptureSession.canAddInput(videoInput)) {
        qrCaptureSession.addInput(videoInput)
      } else {
        print("Error: Unable to initialize back camera 3")
        return
      }
      
      let metadataOutput = AVCaptureMetadataOutput()
      
      if (qrCaptureSession.canAddOutput(metadataOutput)) {
        qrCaptureSession.addOutput(metadataOutput)
        
        metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        metadataOutput.metadataObjectTypes = [.qr]
      } else {
        print("Error: Unable to initialize back camera 4")
        return
      }
      qrStillImageOutput = AVCapturePhotoOutput()
      if (qrCaptureSession.canAddOutput(qrStillImageOutput)) {
        qrCaptureSession.addOutput(qrStillImageOutput)
      } else {
        print("Error: Unable to initialize back camera 4")
        return
      }
      
      backCamera.unlockForConfiguration()
      setupLivePreview()
    } catch {
      print("Error: Unable to initialize back camera - \(error.localizedDescription)")
    }
  }
  
  @objc private func hideCameraCb() {
    if qrCode.isEmpty {
      cameraButton.isHidden = true
      infoButtonLabel.isHidden = true
    } else {
      cameraButton.isHidden = false
      infoButtonLabel.isHidden = false
    }
  }
  
  // Callback function when return is pressed on QR text field
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if let text = qrTxt.text, !text.isEmpty {
      qrCode = text
      scanButton.isEnabled = true
    }
    return true
  }
  
  // Configure QR detection on background
  private func setupLivePreview() {
    qrVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: qrCaptureSession)
    qrVideoPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
    qrVideoPreviewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.landscapeRight
    cameraView.layer.addSublayer(qrVideoPreviewLayer)
    
    let globalQueue = DispatchQueue.global(qos: .background)
    globalQueue.async {
      self.qrCaptureSession.startRunning()
      DispatchQueue.main.async {
        self.qrVideoPreviewLayer.frame = self.cameraView.bounds
      }
    }
  }
  
  internal func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
    if let metadataObjects = metadataObjects as? [AVMetadataObject], !metadataObjects.isEmpty {
      if let metadataObject = metadataObjects.first as? AVMetadataMachineReadableCodeObject,
         metadataObject.type == AVMetadataObject.ObjectType.qr {
        qrCode = metadataObject.stringValue ?? ""
        DispatchQueue.main.async {
          self.qrTxt.text = metadataObject.stringValue
          self.qrTxt.isHidden = false;
          self.cameraButton.isHidden = false
          self.infoButtonLabel.isHidden = false
        }
      }
    } else {
      print("QR not found")
      qrCode = ""
      DispatchQueue.main.async {
        self.qrTxt.text = ""
        self.cameraButton.isHidden = true
        self.infoButtonLabel.isHidden = true
      }
    }
  }
  
  // Handle photo acquisition
  func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
    guard let imageData = photo.fileDataRepresentation(), !qrCode.isEmpty else {
      return
    }
    
    let date = Date()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyyMMdd_HHmmss"
    
    // Extract date to use as folder name
    scanDir = dateFormatter.string(from: date)
    
    // Setup paths and filenames.
    guard let docDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
    
    let qrPhotoPath = docDir.appendingPathComponent("\(mainScanDir)/\(scanDir)/QRImage.jpg")
    
    let fileManager = FileManager.default
    let dir = docDir.appendingPathComponent("\(mainScanDir)/\(scanDir)")
    try? fileManager.createDirectory(at: dir, withIntermediateDirectories: true, attributes: nil)
    
    // Write to disk
    try? imageData.write(to: qrPhotoPath)
    
    //Crear .txt que contenga el string del qr
    let qrCodePath = docDir.appendingPathComponent("\(mainScanDir)/\(scanDir)/QRCode.txt")
    try? qrCode.write(to: qrCodePath, atomically: true, encoding: .utf8)
    
    // Enable scan operation after QR image is stored
    scanButton.isEnabled = true
    
    DispatchQueue.main.async {
      self.dismissView()
    }
  }
  
  @IBAction func takeQRPhoto(_ sender: Any) {
    if !qrCode.isEmpty {
      print("Taking Photo")
      let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
      qrStillImageOutput.capturePhoto(with: settings, delegate: self)
    } else {
      print("QR code not available. Not taking a photo")
    }
  }
  
  @objc func dismissView() {
    // Clear for next iteration
    qrCode = ""
    
    // Stop camera capture session
    qrCaptureSession.stopRunning()
    
    setupCamera()
    
    let storyboard = UIStoryboard(name: "ScanViewController", bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: "ViewController")
    let viewController = vc as! ViewController
    viewController.scanDir = scanDir
    self.present(viewController, animated: true)
  }
}
